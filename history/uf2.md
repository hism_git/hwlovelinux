## 10 nov 2020

La comanda lsblk és per extreure informació de les particions, sistemes de fitxers i punts de muntatge:

```
[professor@localhost ~]$ lsblk 
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0  97.1M  1 loop /var/lib/snapd/snap/core/9993
loop1    7:1    0  17.9M  1 loop /var/lib/snapd/snap/pdftk/9
sda      8:0    0 111.8G  0 disk 
├─sda1   8:1    0   105G  0 part /
└─sda2   8:2    0     5G  0 part [SWAP]
sdb      8:16   0 465.8G  0 disk 
├─sdb1   8:17   0     1K  0 part 
└─sdb5   8:21   0 465.8G  0 part 
sr0     11:0    1  1024M  0 rom  
```

Alias per crear una comanda que mostri columnes interessants d'un disk que m'ofereix la comanda lsblk:

```
[root@localhost ~]# alias lsdisk='lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MOUNTPOINT'
[root@localhost ~]# lsdisk 
NAME   MODEL                     FSTYPE     SIZE TYPE MOUNTPOINT
loop0                            squashfs  97.1M loop /var/lib/snapd/snap/core/9993
loop1                            squashfs  17.9M loop /var/lib/snapd/snap/pdftk/9
sda    Samsung_SSD_850_EVO_120GB          111.8G disk 
├─sda1                           ext4       105G part /
└─sda2                           swap         5G part [SWAP]
sdb    TOSHIBA_DT01ACA050                 465.8G disk 
├─sdb1                                        1K part 
└─sdb5                           ext4     465.8G part 
sr0    HL-DT-ST_DVDRAM_GH24NSC0            1024M rom  
````
## 11 nov

generar clau
```
[professor@localhost ~]$ ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/professor/.ssh/id_rsa): 
/home/professor/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/professor/.ssh/id_rsa
Your public key has been saved in /home/professor/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:XKrHi4Kh23TXQR3yKJtLru1uNjhb51En9SU4/OvMKZo professor@localhost.localdomain
The key's randomart image is:
+---[RSA 3072]----+
|        . .      |
|         = o .   |
|      . o + = . .|
|       * o . + o |
|      + S o . o  |
|  .  o = o o   . |
| ..o..* *     .  |
|.o..+=+= o ..+ . |
|... oO=.o E. .=  |
+----[SHA256]-----+
[professor@localhost ~]$ cat .ssh/id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDVZHaJWpByfrahW1NwuHdQni0Gg67Um5aqcMfpAUYzD44U0fqPz4ga3V0kk5rErWLVd7MwPgEk1OZ0Vz9VwA7QNkAK2f7/K5UXxs2HkP6dbissEWYakfAhj6T29xd/jfayWbk0yzMPgE1NAhr5KyA/LLh2o6DwIlIyaigunVgkXrmSTqyqUKj9N27wxMIxv99LNn1vr3hX0QHBWG2C57TSiUQ3WVsY3FaCfS6OofTYNfeihd3QLmKea9Cm+YMwuj7qYjDPEvSvlj6NY0r4jHkxcCYXQssahS5+U/m4WhWkvKcvGy5E8sM7vFeAPRykRw6Ghqgsh8Pctl1mOmAl+WEKSHMBkehcOn8phPWJCsJ+hc1ehz5KcPmT9zxi04zMRLRjMyhiPMoN7J26hWHPlgqygGFATa9ZKt5SLvFo+rH7tlsLHgMAXJbDZyCEC3r1IQIKqc/emMJegpAuC250Hfka+JiIZMCup3NJIwPPAqf/6OyxikoyN1kKmbFjuJww0a0= professor@localhost.localdomain
```

Copiem la clau al gitlab i fem un git clone per baixar el repository:
```
[professor@localhost ~]$ git clone git@gitlab.com:hism_git/hwlovelinux.git
Cloning into 'hwlovelinux'...
remote: Enumerating objects: 75, done.
remote: Counting objects: 100% (75/75), done.
remote: Compressing objects: 100% (64/64), done.
remote: Total 75 (delta 7), reused 26 (delta 1), pack-reused 0
Receiving objects: 100% (75/75), 2.88 MiB | 5.08 MiB/s, done.
Resolving deltas: 100% (7/7), done.

```

per baixar les actualitzacions del repositori fem un git pull
```
[professor@localhost ~]$ cd hwlovelinux
[professor@localhost hwlovelinux]$ git pull
remote: Enumerating objects: 7, done.
remote: Counting objects: 100% (7/7), done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 6 (delta 2), reused 1 (delta 0), pack-reused 0
Unpacking objects: 100% (6/6), 584 bytes | 584.00 KiB/s, done.
From gitlab.com:hism_git/hwlovelinux
   5ff9019..02a478c  master     -> origin/master
Updating 5ff9019..02a478c
Fast-forward
 index.md | 5 +++++
 1 file changed, 5 insertions(+)
 create mode 100644 index.md
 

[professor@localhost hwlovelinux]$ ls
ca  history  img  index.md  README.md

```

Si realitzo canvis en local, he de pujar aquest canvis fent un 
commit i un push

```

```

### ordre lshw

La ordre lshw dona molta informació, per filtrar aquesta inforamció podem hem mirat el help y el manual

```
lshw --help
man lshw
```

La opció:

 	-class CLASS    only show a certain class of hardware
	-C CLASS        same as '-class CLASS'

no quedava clar i hem tingut que anar al manual

```
       -class class
              Only show the given class of hardware. class can  be  found  using  lshw
              -short or lshw -businfo.
``` 

Llavors lo millor és fer servir els valors de la columna class
```
H/W path           Device      Class          Description
=========================================================
                               system         To be filled by O.E.M. (To be filled by O.E
/0                             bus            H61M-USB3H

```

i amb el class **disk** ja ho tenim:

```
lshw -class disk
```
Versió resumida:

```
[root@localhost ~]# lshw -short -c disk 
H/W path           Device      Class          Description
=========================================================
/0/100/1f.2/0      /dev/sda    disk           120GB Samsung SSD 850
/0/100/1f.2/1      /dev/sdb    disk           500GB TOSHIBA DT01ACA0
/0/100/1f.5/0.0.0  /dev/cdrom  disk           DVDRAM GH24NSC0

```

```
  436  date
  437  date; cp /mnt/prova2G /root/prova2G; date
  438  rm -f /root/prova2G; date; cp /mnt/prova2G /root/prova2G; date
  439  python
  440  lshw
  441  lshw --help
  442  lshw --businfo
  443  lshw -businfo
  444  lshw --help
  445  lshw -class
  446  lshw -class pepe
  447  man lshw
  448  lshw -businfo
  449  lshw --help
  450  lshw --short
  451  lshw -short
  452  lshw -class disk
  453  lshw --help

``` 

