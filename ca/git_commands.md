## 10 nov 2020



generar clau ssh
```
[professor@localhost ~]$ ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/professor/.ssh/id_rsa): 
/home/professor/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/professor/.ssh/id_rsa
Your public key has been saved in /home/professor/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:XKrHi4Kh23TXQR3yKJtLru1uNjhb51En9SU4/OvMKZo professor@localhost.localdomain
The key's randomart image is:
+---[RSA 3072]----+
|        . .      |
|         = o .   |
|      . o + = . .|
|       * o . + o |
|      + S o . o  |
|  .  o = o o   . |
| ..o..* *     .  |
|.o..+=+= o ..+ . |
|... oO=.o E. .=  |
+----[SHA256]-----+
[professor@localhost ~]$ cat .ssh/id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDVZHaJWpByfrahW1NwuHdQni0Gg67Um5aqcMfpAUYzD44U0fqPz4ga3V0kk5rErWLVd7MwPgEk1OZ0Vz9VwA7QNkAK2f7/K5UXxs2HkP6dbissEWYakfAhj6T29xd/jfayWbk0yzMPgE1NAhr5KyA/LLh2o6DwIlIyaigunVgkXrmSTqyqUKj9N27wxMIxv99LNn1vr3hX0QHBWG2C57TSiUQ3WVsY3FaCfS6OofTYNfeihd3QLmKea9Cm+YMwuj7qYjDPEvSvlj6NY0r4jHkxcCYXQssahS5+U/m4WhWkvKcvGy5E8sM7vFeAPRykRw6Ghqgsh8Pctl1mOmAl+WEKSHMBkehcOn8phPWJCsJ+hc1ehz5KcPmT9zxi04zMRLRjMyhiPMoN7J26hWHPlgqygGFATa9ZKt5SLvFo+rH7tlsLHgMAXJbDZyCEC3r1IQIKqc/emMJegpAuC250Hfka+JiIZMCup3NJIwPPAqf/6OyxikoyN1kKmbFjuJww0a0= professor@localhost.localdomain
```

Copiem la clau al gitlab i fem un git clone per baixar el repository:
```
[professor@localhost ~]$ git clone git@gitlab.com:hism_git/hwlovelinux.git
Cloning into 'hwlovelinux'...
remote: Enumerating objects: 75, done.
remote: Counting objects: 100% (75/75), done.
remote: Compressing objects: 100% (64/64), done.
remote: Total 75 (delta 7), reused 26 (delta 1), pack-reused 0
Receiving objects: 100% (75/75), 2.88 MiB | 5.08 MiB/s, done.
Resolving deltas: 100% (7/7), done.

```

per baixar les actualitzacions del repositori fem un git pull
```
[professor@localhost ~]$ cd hwlovelinux
[professor@localhost hwlovelinux]$ git pull
remote: Enumerating objects: 7, done.
remote: Counting objects: 100% (7/7), done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 6 (delta 2), reused 1 (delta 0), pack-reused 0
Unpacking objects: 100% (6/6), 584 bytes | 584.00 KiB/s, done.
From gitlab.com:hism_git/hwlovelinux
   5ff9019..02a478c  master     -> origin/master
Updating 5ff9019..02a478c
Fast-forward
 index.md | 5 +++++
 1 file changed, 5 insertions(+)
 create mode 100644 index.md
 

[professor@localhost hwlovelinux]$ ls
ca  history  img  index.md  README.md

```

Si realitzo canvis en local, hem de fer servir 5 ordres importants:
- git status (comprovar si hi ha canvis)
- git diff (veure el detall dels canvis)
- git add . (si has creat nous fitxers, afegeix tots els nous fitxers al repositori)
- git commit -am "comentari" (fa una nova versió del repositori i has de possar un comentari)
- git push (puja els comenaris)





